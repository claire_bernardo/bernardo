# language: en
Feature: Connexion-jpet-store

	Scenario Outline: Connexion-jpet-store
		Given Je suis sur la page login du site jpet store
		And je clique sur le lien sign in
		And je suis redirigé vers la page d'authentification
		When je renseigne le champs username avec mon <login>
		And je renseigne le champs password avec le mot de passe <mdp>
		Then je suis connecté

		@informations-login
		Examples:
		| login | mdp |
		| "j2ee" | "j2ee" |